const express = require("express")

// Khởi tạo App
const app = new express()

// Khai báo cổng
const port = 8000
// Hiển thị hình ảnh
app.use(express.static(__dirname + "/views"))
// Get / dẫn đến trang index.html
app.get("/", (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/index.html"))
})
// Listen port
app.listen(port, () => {
    console.log("App listen by port: ", port)
})